const express = require("express");
const path = require("path");

const app = express();
var router = express.Router();

app.use("/", router);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

let port = 8080;

app.listen(port, () => {
  console.log("Server is running on port " + port);
});
app.get("/", (req, res) => {
  res.status(200).send("Hello world!");
});

router.route("/croco").get((request, response) => {
  response.sendFile(path.join(path.resolve(), "../front-end/index.html"));
});
